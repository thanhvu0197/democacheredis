﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Caching.Distributed;
using System.Text;
using EasyCaching.Core;

namespace Demo.Controllers
{
    [ApiController]
    [Route("/Redis")]
    public class RedisController : ControllerBase
    {
        private IEasyCachingProvider cachingProvider;
        private IEasyCachingProviderFactory cachingProviderFactory;

        private IEasyCachingProvider cachingProviderRead;
        private IEasyCachingProviderFactory cachingProviderFactoryRead;
        public RedisController(IEasyCachingProviderFactory cachingProviderFactory)
        {
            this.cachingProviderFactory = cachingProviderFactory;
            this.cachingProvider = this.cachingProviderFactory.GetCachingProvider("redis1");

            this.cachingProviderFactoryRead = cachingProviderFactory;
            this.cachingProviderRead = this.cachingProviderFactory.GetCachingProvider("redis2");
        }

        [HttpGet("Set")]
        public IActionResult SetItemInQueue()
        {
            string Testkey = "Key";
            for (int i = 0; i <= 1000; i++)
            {                
                this.cachingProvider.Set(Testkey+i, "Thanh Test Value", TimeSpan.FromDays(100));
            }
            return Ok();
        }

        [HttpGet("Get")]
        public IActionResult GetItemInQueue()
        {
            string Testkey = "Key";

            List<string> ListItem = new List<string>();
            for (int i = 0; i <= 1000; i++)
            {
                 var item = this.cachingProviderRead.Get<string>(Testkey + i);
                 string test = item.ToString();
                ListItem.Add(test);
            }
            return Ok(ListItem);
        }
    }
}